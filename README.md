# Dos pasos por delante

https://dospasospordelante.org/ Es un proyecto con base abierta que busca ayudar en la prevención de soluciones a los proyectos que están por venir (o podrían estar por venir) en la situación actual respecto al COVID-19.
Nace desde local (Córdoba) pero desde el minuto 0 es exportable y conectable a lo global, por eso lanzamos en abierto.

# plugin-2xd-webmap

Este repositorio contiene un plugin para leer los datos desde AWS o Gsheet y pintarlos en un mapa de WordPress.

# Ayuda

Si quieres ayudar, hay grupo en Telegram, entra sólo si puedes y quieres ayudar con Plugin WordPress, por favor:
https://t.me/joinchat/AEvnSBjYWP_btCj63E3U2g


