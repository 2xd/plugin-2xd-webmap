<?php

class DosXD_WebComponent{
	function __construct(){
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ), 10 );
		add_shortcode( '2xd-map', array( $this, 'shortcode' ) );
	}

	function enqueue() {
		global $post, $wpdb;

		if( !is_object( $post ) )
			return;

		$shortcode_found = false;

		if ( has_shortcode($post->post_content, '2xd-map') ) {
			$shortcode_found = true;
		} else if ( isset($post->ID) ) {
			$result = $wpdb->get_var( $wpdb->prepare(
				"SELECT count(*) FROM $wpdb->postmeta " .
				"WHERE post_id = %d and meta_value LIKE '%%2xd-map%%'", $post->ID ) );
			$shortcode_found = ! empty( $result );
		}

		if( !$shortcode_found )
			return;

		foreach ( glob( plugin_dir_path( __FILE__ ) . "css/*.css" ) as $file ) {
			$file_info = pathinfo( $file );
		    wp_enqueue_style( $file_info['filename'], plugins_url( '/css/' . $file_info['basename'], __FILE__ ), array(), DOSXD_VERSION );
		}

		foreach ( glob( plugin_dir_path( __FILE__ ) . "js/*.js" ) as $file ) {
		    $file_info = pathinfo( $file );
		    wp_enqueue_script( $file_info['filename'], plugins_url( '/js/' . $file_info['basename'], __FILE__ ), array(), DOSXD_VERSION );
		}
	}

	function shortcode( $atts ){
		$atts = shortcode_atts( array(
			'titulacion' => '',
		), $atts, '2xd-map' );

		ob_start();
		
		include( 'web-component.html' );

		$cadena = ob_get_contents();
		ob_end_clean();

		return $cadena;
	}
}

new DosXD_WebComponent;