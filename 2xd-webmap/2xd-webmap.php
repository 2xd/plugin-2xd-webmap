<?php
/**
 * Plugin Name:       2xd-webmap
 * Plugin URI:        https://dospasospordelante.org/
 * Description:       Dos pasos por delante integración en el shortcode
 * Version:           0.1
 * Requires at least: 5.2
 * Requires PHP:      5.6
 * Author:            2 Pasos Por Delante
 * Author URI:        https://dospasospordelante.org/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       2xd-webmap
 * Domain Path:       /languages
 */

define( 'DOSXD_VERSION', '0.1' );

include( 'class-webcomponent.php' );